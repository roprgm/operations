#!/usr/bin/python
# -*- coding: utf-8 -*-

from operations.src.base import Operation, IterInput

from utils import get_path


class Merge(Operation):
    def __init__(self, stages, key="_id"):
        super(Operation, self).__init__()
        self.stages = stages
        self.key = key

    def run(self, iterable=None):
        items = list(iterable) if iterable else None

        result = {}
        for stage in self.stages:
            for key, value in stage.dict(self.key, items).iteritems():
                result.setdefault(key, {}).update(value)

        return IterInput(result.values())

class RenameKeys(Operation):
    def __init__(self, key_map, **config):
        super(RenameKeys, self).__init__(**config)
        self.key_map = key_map

    def run(self, iterable=None):
        for doc in iter(iterable):
            new_doc = dict(doc)
            for new_key, old_key in self.key_map.items():
                new_doc[new_key] = new_doc.pop(old_key)
            yield new_doc

class RemoveKeys(Operation):
    def __init__(self, keys, **config):
        super(RemoveKeys, self).__init__(**config)
        self.keys = keys

    def run(self, iterable=None):
        for doc in iter(iterable):
            new_doc = dict(doc)
            for key in self.keys:
                if key in new_doc:
                    new_doc.pop(key)
            yield new_doc

class RemoveNulls(Operation):
    def run(self, iterable=None):
        for doc in iterable:
            new_doc = dict()
            for key, value in doc.iteritems():
                if not value is None:
                    new_doc[key] = value
            yield new_doc

class Project(Operation):
    def __init__(self, projection, project_default=False, **config):
        super(Project, self).__init__(**config)
        self.projection = projection
        self.project_default = project_default

    def run(self, iterable=None):

        def project(doc, projection, new_key):

            if callable(projection):
                return projection(doc)

            elif isinstance(projection, basestring):
                if "{" in projection:
                    return projection.format(**doc)
                elif "%" in projection:
                    return projection % doc
                else:
                    return get_path(doc, projection)

            elif isinstance(projection, list):
                value = []
                for p in projection:
                    v = project(doc, p, new_key)
                    if v: value.append(v)
                return value

            elif isinstance(projection, dict):
                value = {}
                for k,p in projection.items():
                    v = project(doc, p, new_key)
                    if v: value[k] = v
                return value

            elif bool(projection):
                return doc.get(new_key)

        for doc in iter(iterable):
            new_doc = dict(doc) if self.project_default else dict()
            for new_key, projection in self.projection.items():
                if projection == 0 and new_key in new_doc:
                    del new_doc[new_key]
                else:
                    value = project(doc, projection, new_key)
                    if not value is None:
                        new_doc[new_key] = value
            yield new_doc

class DictInput(Operation):
    def __init__(self, data, key="_id", **config):
        super(DictInput, self).__init__(**config)
        self.key = key
        self.data = data

    def run(self, iterable=None):
        it = self.data.iteritems()
        for key, item in it:
            doc = dict(item)
            doc[self.key] = key
            yield doc
