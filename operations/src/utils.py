from datetime import datetime, timedelta

# Date range generator
def date_range(start, stop, step=timedelta(days=1), format=None):
    current = start
    while current < stop:
        yield current.strftime(format) if format else current
        current += step

def bucketize(generator, size=1000):
    env = { "bucket": [] }
    for item in generator:
        bucket = env["bucket"]
        bucket.append(item)
        if len(bucket) == size:
            yield bucket
            env["bucket"] = []
    if env["bucket"]:
        yield env["bucket"]

def get_path(obj, path, default=None):
    try:
        for arg in path.split("."):
            obj = obj[arg]
        return obj
    except KeyError:
        return default

