#!/usr/bin/python
# -*- coding: utf-8 -*-

from operations.src.base import Operation
from pymongo import MongoClient
from .utils import bucketize

class DefaultConnector(object):

    def connect(self, host, **kwargs):
        return MongoClient(host=host, **kwargs)

class MongoUpdater(object):

    def __init__(self, match):
        self.match = match
        self.add_to_set_ops = []
        self.sort_ops = []
        self.ops = {}
        self.data = {}

    def set_op(self, key, op):
        self.ops[key] = op

    def update(self, other):
        for key, value in other.items():
            self.set(key, value)
        return self

    def set(self, key, value):
        if not value is None:
            self.ops[key] = "$set"
            self.data[key] = value
        return self

    def unset(self, key):
        self.ops[key] = "$unset"
        self.data[key] = ""
        return self

    def min(self, key, value):
        if not value is None:
            self.ops[key] = "$min"
            self.data[key] = min(value, self.data.get(key, value))
        return self

    def max(self, key, value):
        if not value is None:
            self.ops[key] = "$max"
            self.data[key] = max(value, self.data.get(key, value))
        return self

    def push(self, key, *values, **kwargs):
        current = self.data.get(key, [])
        for value in values:
            if not value is None:
                current.append(value)
        if current:
            self.ops[key] = "$push"
            self.data[key] = current
            if kwargs.get("sort"):
                self.sort(key, kwargs.get("sort"))
        return self

    def add_to_set(self, key, *values, **kwargs):
        pk = kwargs.get("pk")
        currents = self.data.get(key, [])
        for value in values:
            if not value is None:
                if pk:
                    self.add_to_set_ops.append((key, value, pk))
                    currents = [c for c in currents if not c[pk] == value[pk]]
                    currents.append(value)
                else:
                    if not value in currents:
                        currents.append(value)
        if currents:
            self.ops[key] = "$addToSet"
            self.data[key] = currents
            if kwargs.get("sort"):
                self.sort(key, kwargs.get("sort"))
        return self

    def sort(self, key, sort):
        if not isinstance(sort, dict):
            sort = { sort:1 }
        self.sort_ops.append((key, sort))
        return self

    def current_date(self, key):
        self.ops[key] = "$currentDate"
        self.data[key] = True
        return self

    # build
    def build_ops(self):
        ops = []
        update = {}
        for key, value, pk in self.add_to_set_ops:
            ops.append({ "$pull": { key: { pk:value[pk] } } })
        for key, op in self.ops.items():
            value = self.data.get(key)
            if op in ["$push", "$addToSet"]:
                value = { "$each":value }
            update.setdefault(op, {})[key] = value
        if update:
            ops.append(update)
        for key, sort in self.sort_ops:
            ops.append({ "$push": { key: { "$each": [], "$sort": sort } } })
        return ops

    def on_bulk(self, bulk):
        return bulk.update(self)

    def __repr__(self):
        return "MongoUpdater(%s) %s" % (str(self.match), str(self.data))


class MongoBulk(object):

    def __init__(self, collection):
        self.collection = collection
        self.bulk = collection.initialize_ordered_bulk_op()
        self.op_count = 0

    # Replacements
    def replace(self, replacement, spec=None, upsert=True, multi=False, key=None):

        if key:
            match = { key:replacement[key] }
        else:
            if isinstance(spec, dict): match = spec
            elif spec: match = {"_id":spec}
            else: match = {"_id":replacement["_id"]}

        op = self.bulk.find(match)
        if upsert: op = op.upsert()
        if multi: op.replace(replacement)
        else: op.replace_one(replacement)

        self.op_count += 1

    def replace_docs(self, docs, key="_id", upsert=True, multi=False):
        for _id, doc in docs.iteritems():
            self.replace(dict(doc), {key:_id}, upsert=upsert, multi=multi)

    def replace_doc(self, doc, spec=None, upsert=True, multi=False):
        self.replace(dict(doc), spec, upsert=upsert, multi=multi)

    # Updates
    def update(self, update_op, spec=None, upsert=False, multi=False):
        if isinstance(update_op, MongoUpdater):
            ops = update_op.build_ops()
            for op in ops:
                self.update(op, spec=update_op.match, upsert=upsert, multi=multi)

        elif update_op:
            if isinstance(spec, dict): match = spec
            elif spec: match = {"_id":spec}
            else: match = {"_id":update_op.get["_id"]}

            op = self.bulk.find(match)
            if upsert: op = op.upsert()
            if multi: op.update(update_op)
            else: op.update_one(update_op)

            self.op_count += 1

    def update_docs(self, docs, key="_id", upsert=False, multi=False):
        for _id, doc in docs.iteritems():
            self.update(doc.update_dict(), {key:_id}, upsert=upsert, multi=multi)

    def update_doc(self, doc, spec=None, upsert=False, multi=False):
        self.update(doc.update_dict(), spec, upsert=upsert, multi=multi)

    # Execute
    def execute(self):
        if self.op_count:
            result = self.bulk.execute()
            if result["nMatched"]:
                return "+%s ~%s/%s (%i)" % (result["nUpserted"], result["nModified"], result["nMatched"], self.op_count)
            return "+%s (%i)" % (result["nUpserted"], self.op_count)
        return "+%s (%i)" % (0,0)

class MongoIndex(object):
    def __init__(self, key_or_list, **kwargs):
        self.key = key_or_list
        self.kwargs = kwargs

class MongoOperation(Operation):

    default_host = "mongodb://localhost/test"

    @classmethod
    def using(cls, host):
        cls.default_host = host

    def __init__(self, collection, host=None, connector=None, **config):
        super(MongoOperation, self).__init__(**config)

        self.host = host or self.default_host
        self.collection = collection or "test"
        self.connector = connector or DefaultConnector()
        self.connection = None

    def connect(self, **kwargs):

        self.connection = self.connector.connect(self.host, **kwargs)

        if "/" in self.collection:
            database, collection = self.collection.split("/")
            return self.connection[database][collection]

        return self.connection.get_default_database()[self.collection]

    def disconnect(self):
        if self.connection:
            self.connection.close()
            self.connection = None

class MongoEnsureIndex(MongoOperation):

    def run(self, iterator=None):
        collection = self.connect()

        for index in iterator:
            if isinstance(index, MongoIndex):
                collection.ensure_index(index.key, **index.kwargs)
            elif isinstance(index, basestring):
                collection.ensure_index(index)
            elif isinstance(index, list):
                collection.ensure_index(index)
            elif isinstance(index, dict):
                keys = index.pop("keys")
                collection.ensure_index(keys, **index)

        self.disconnect()

class MongoFind(MongoOperation):

    def match(self, match):
        self.config.setdefault("match", {}).update(match)
        return self

    def project(self, project):
        if isinstance(project, list):
            project = { key:1 for key in project }
        self.config.setdefault("project", {}).update(project)
        return self

    def sort(self, sort):
        self.config["sort"] = sort
        return self

    def limit(self, limit):
        self.config["limit"] = limit
        return self

    def run(self, cursor=None):
        collection = self.connect(slave_okay=True)

        op = collection.find(self.config.get("match", {}), self.config.get("project"))

        if self.config.get("sort"):
            op.sort(self.config.get("sort"))

        if self.config.get("limit"):
            op.limit(self.config.get("limit"))

        for doc in op:
            yield doc

        self.disconnect()

class MongoAggregate(MongoOperation):

    def __init__(self, collection, pipeline=None, **config):
        super(MongoAggregate, self).__init__(collection, **config)
        self.pipeline = pipeline or []

    def run(self, iterable=None):
        collection = self.connect(slave_okay=True)

        for doc in collection.aggregate(self.pipeline, cursor={}, allowDiskUse=True):
            yield doc

        self.disconnect()

    def match(self, match):
        self.pipeline.append({ "$match": match })
        return self

    def project(self, project):
        self.pipeline.append({ "$project": project })
        return self

    def group(self, group):
        self.pipeline.append({ "$group": group })
        return self

    def unwind(self, unwind):
        self.pipeline.append({ "$unwind": unwind })
        return self

    def sort(self, sort):
        self.pipeline.append({ "$sort": sort })
        return self

    def limit(self, limit):
        self.pipeline.append({ "$limit": limit })
        return self

class MongoUpdate(MongoOperation):

    def updater(self, doc):
        if isinstance(doc, MongoUpdater):
            return doc
        elif self.config.get("key"):
            key = self.config.get("key")
            updater = MongoUpdater({key: doc[key]})
            updater.update(doc)
            return updater
        elif self.config.get("updater"):
            return self.config.get("updater")(doc)
        else:
            print "WARNING: No updater or key defined"

    def run(self, cursor=None):
        collection = self.connect()

        upsert = self.config.get("upsert", False)

        for bucket in bucketize(cursor, self.config.get("bulk_size", 1000)):

            bulk = MongoBulk(collection)

            for doc in bucket:
                updater = self.updater(doc)
                if updater:
                    bulk.update(updater, upsert=upsert)

            result = bulk.execute()
            print self.collection, result

        self.disconnect()

class MongoReplace(MongoOperation):

    def __init__(self, collection, key=None, upsert=True, clear=False, bulk_size=1000, **config):
        super(MongoReplace, self).__init__(collection, **config)
        self.bulk_size = bulk_size
        self.upsert = upsert
        self.clear = clear
        self.key = key

    def run(self, cursor=None):
        collection = self.connect()

        if self.upsert and self.clear:
            collection.remove()

        for bucket in bucketize(cursor, self.bulk_size):

            bulk = MongoBulk(collection)

            for doc in bucket:
                bulk.replace(doc, key=self.key, upsert=self.upsert)

            print self.collection, bulk.execute()

        self.disconnect()


