#!/usr/bin/python
# -*- coding: utf-8 -*-

from utils import bucketize

from elasticsearch.client import Elasticsearch
from elasticsearch.helpers import bulk, scan, streaming_bulk

from base import Operation

from logging import getLogger
logger = getLogger("elastic")

class ElasticOperation(Operation):

    @classmethod
    def using(cls, host):
        cls.default_host = host

    def __init__(self, index=None, server=None, timeout=30, **config):
        super(ElasticOperation, self).__init__(**config)
        self.es = Elasticsearch(server or self.default_host, timeout=timeout)
        self.index = index

    def search(self, **kwargs):
        return self.es.search(**kwargs)

    def bulk(self, actions, **kwargs):
        return bulk(self.es, actions, **kwargs)

    def streaming_bulk(self, actions, **kwargs):
        return streaming_bulk(self.es, actions, **kwargs)

    def scan(self, query, bucket_size=None, debug=True, **kwargs):

        total_docs = self.search(body=query, search_type="count", **kwargs)["hits"]["total"]
        logger.info("Starting ElasticReader search of %i docs" % total_docs)

        cursor = scan(self.es, query=query, **kwargs)

        if bucket_size:
            cursor = bucketize(cursor, bucket_size)

        count = 0
        total_mod = max(1, total_docs/(bucket_size or 1)/10000)
        for item in cursor:
            yield item
            if debug and total_docs:
                count += 1
                if count % total_mod == 0:
                    logger.info("ElasticReader search %0.2f%%" % ((100.0 * count * (bucket_size or 1)) / total_docs))

class ElasticFind(ElasticOperation):

    def __init__(self, index, query, bulk_size=100, **config):
        super(ElasticFind, self).__init__(index, **config)
        self.bulk_size = bulk_size
        self.query = query

    def run(self, iterable=None):
        for item in self.scan(self.query, self.bulk_size):
            yield item

class ElasticIndex(ElasticOperation):
    def __init__(self, index, type=None, bulk_size=100, **config):
        super(ElasticIndex, self).__init__(index, **config)
        self.bulk_size = bulk_size
        self.type = type

    def run(self, iterable=None):

        for bucket in bucketize(iterable, self.bulk_size):
            ops = []
            for doc in bucket:
                ops.append({
                    "_id": doc.get("_id"),
                    "_type": doc.get("_type", self.type),
                    "_source": doc,
                })
            success, failed = self.bulk(ops, index=self.index)
            print "Index", success, failed

