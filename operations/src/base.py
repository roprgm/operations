#!/usr/bin/python
# -*- coding: utf-8 -*-

from utils import bucketize, get_path
from operator import itemgetter
from collections import OrderedDict
import itertools

class Operation(object):

    def __init__(self, **config):
        self.config = config
        self.running = False

    def __call__(self, iterable=None):
        return self.run(iterable)

    def __iter__(self):
        return iter(self.run())

    def _start(self):
        self.running = True

    def _stop(self):
        self.running = False

    def run(self, iterable=None):
        return []

    def list(self):
        return list(self)

    def dict(self, key="_id", iterable=None):
        return { item[key]:item for item in self(iterable) }

    def ordered_dict(self, key="_id"):
        out = OrderedDict()
        for item in self:
            out[item[key]] = item
        return out

    def __or__(self, other):
        return Pipeline([self, other])

    def limit(self, limit):
        return IterInput(itertools.islice(self, 0, limit))

    def skip(self, skip):
        return IterInput(itertools.islice(self, skip, None))

    def sort(self, key=None, reverse=False):
        return Pipeline([self, Sort(key=key, reverse=reverse)])

class BulkOperation(Operation):

    def __init__(self, bulk_size=10, **config):
        super(BulkOperation, self).__init__(**config)
        self.bulk_size = bulk_size

    def process(self, bucket):
        return bucket

    def run(self, iterable=None):
        self._start()
        for bucket in bucketize(iterable, self.bulk_size):
            for result in self.process(bucket):
                if not result is None:
                    yield result
        self._stop()


# Flow operations

class Pipeline(Operation):
    #
    # >--O1-->--O2-->
    #
    def __init__(self, stages, **config):
        super(Pipeline, self).__init__(**config)
        self.stages = stages

    def run(self, iterable=None):
        for stage in self.stages:
            if isinstance(stage, Operation):
                iterable = stage.run(iterable)
            else:
                iterable = iter(stage)
        return iterable

class Split(Operation):
    def __init__(self, operations, **config):
        super(Split, self).__init__(**config)
        self.operations = [op if isinstance(op, Operation) else IterInput(op) for op in operations]

    def run(self, iterable=None):
        if iterable:
            iterables = itertools.tee(iterable, len(self.operations))
            iresults = [op.run(it) for op,it in zip(self.operations, iterables)]
        else:
            iresults = [op.run() for op in self.operations]

        for item in itertools.izip_longest(*iresults):
            yield item

class Concat(Operation):
    def __init__(self, operations, **config):
        super(Concat, self).__init__(**config)
        self.operations = [op if isinstance(op, Operation) else IterInput(op) for op in operations]

    def run(self, iterable=None):
        if iterable:
            iterables = itertools.tee(iterable, len(self.operations))
            iresults = [op.run(it) for op, it in zip(self.operations, iterables)]
        else:
            iresults = [op.run() for op in self.operations]

        for iresult in iresults:
            for item in iresult:
                yield item

# Function Operations

class Map(Operation):
    def __init__(self, func, *args, **config):
        super(Map, self).__init__(**config)
        self.func = func
        self.args = args

    def run(self, iterable=None):
        for x in iter(iterable):
            yield self.func(x, *self.args)

class Reduce(Operation):
    def __init__(self, func, initializer=None, **config):
        super(Reduce, self).__init__(**config)
        self.initializer = initializer
        self.func = func

    def run(self, iterable=None):
        it = iter(iterable)
        if self.initializer is None:
            self.initializer = next(it)
        accum_value = self.initializer
        for x in it:
            accum_value = self.func(accum_value, x)
        return accum_value

class Filter(Operation):
    def __init__(self, func, *args, **config):
        super(Filter, self).__init__(**config)
        self.args = args
        self.func = func

    def run(self, iterable=None):
        for x in iter(iterable):
            if self.func(x, *self.args):
                yield x

class FilterNone(Operation):
    def run(self, iterable=None):
        for x in iter(iterable):
            if not x is None:
                yield x

class ItemGetter(Map):
    def __init__(self, *items, **config):
        func = itemgetter(*items)
        super(ItemGetter, self).__init__(func, **config)

# Buckets

class Bucketize(Operation):
    def __init__(self, bulk_size=1000,  **config):
        super(Bucketize, self).__init__(**config)
        self.bulk_size = bulk_size

    def run(self, iterable=None):
        for bucket in bucketize(iterable, self.bulk_size):
            yield bucket

class Unbucketize(Operation):
    def run(self, iterable=None):
        for bucket in iterable:
            for x in bucket:
                yield x

# Others

class ConsumeOperation(Operation):
    def __init__(self, **config):
        super(ConsumeOperation, self).__init__(**config)

    def run(self, iterable=None):
        for doc in iterable:
            pass

class Print(Operation):
    def __init__(self, name=None, **config):
        super(Print, self).__init__(**config)
        self.name = name or ""

    def run(self, iterable=None):
        n = 0
        for doc in iter(iterable):
            print n, self.name, doc
            yield doc
            n += 1

class PrintOutput(Operation):
    def __init__(self, name=None, **config):
        super(PrintOutput, self).__init__(**config)
        self.name = name or ""

    def run(self, iterable=None):
        n = 0
        for doc in iter(iterable):
            print n, self.name, doc
            n += 1

class IterInput(Operation):
    def __init__(self, iterable, **config):
        super(IterInput, self).__init__(**config)
        self.iterable = iterable

    def run(self, iterable=None):
        for item in self.iterable:
            yield item

ListInput = IterInput

class Sort(Operation):
    def __init__(self, key=None, reverse=False, **config):
        super(Sort, self).__init__(**config)
        self.key = key
        self.reverse = reverse

    def run(self, iterable=None):
        if not self.key is None and not callable(self.key):
            self.key = itemgetter(self.key)
        for item in sorted(iterable, key=self.key, reverse=self.reverse):
            yield item