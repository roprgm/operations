#!/usr/bin/python
# -*- coding: utf-8 -*-

from base import BulkOperation
from smart_open import smart_open
from utils import bucketize
import io, csv

class S3WriteFile(BulkOperation):

    def __init__(self, bucket, file, bulk_size=100):
        super(S3WriteFile, self).__init__(bulk_size=bulk_size)
        self.s3_uri = "%s/%s" % (bucket, file)

    def run(self, iterable=None):
        with smart_open(self.s3_uri, "wb") as out:
            for bucket in bucketize(iterable, self.bulk_size):
                for item in bucket:
                    out.write(item)

class S3WriteCSV(BulkOperation):
    def __init__(self, bucket, file, fieldnames=None, bulk_size=100):
        super(S3WriteCSV, self).__init__(bulk_size=bulk_size)
        self.s3_uri = "%s/%s" % (bucket, file)
        self.fieldnames = fieldnames

    def run(self, iterable=None):

        it = iter(iterable)

        f = io.BytesIO()
        with smart_open(self.s3_uri, "wb") as fout:

            first = None
            if not self.fieldnames:
                first = it.next()
                self.fieldnames = first.keys()

            writer = csv.DictWriter(f, fieldnames=self.fieldnames, extrasaction="ignore")
            writer.writeheader()
            fout.write(f.getvalue())

            if first:
                writer.writerow(first)

            for bucket in bucketize(it, self.bulk_size):
                f.seek(0)
                f.truncate(0)
                writer.writerows(bucket)
                fout.write(f.getvalue())
        f.close()