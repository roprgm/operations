#!/usr/bin/python
# -*- coding: utf-8 -*-

from base import Operation
import requests

class HTTPGet(Operation):

    def __init__(self, status_codes=None, **config):
        super(HTTPGet, self).__init__(**config)
        self.status_codes = status_codes

    def run(self, iterable=None):
        for url in iterable:
            response = requests.get(url)
            if not self.status_codes or response.status_code in self.status_codes:
                yield response.content