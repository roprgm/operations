from base import Operation
from csv import DictReader, DictWriter
from utils import bucketize

class CSVFileRead(Operation):
    """
        Read CSV files to dictionaries using keys from first line
    """
    def __init__(self, filename,  **config):
        super(CSVFileRead, self).__init__(**config)
        self.filename = filename

    def run(self, iterable=None):
        with open(self.filename) as csvfile:
            reader = DictReader(csvfile)
            for row in reader:
                yield row

class CSVFileWrite(Operation):
    """
        Write CSV files to dictionaries using keys from fieldnames
    """
    def __init__(self, filename, fieldnames=None, bulk_size=100, **config):
        super(CSVFileWrite, self).__init__(**config)
        self.filename = filename
        self.fieldnames = fieldnames
        self.bulk_size = bulk_size

    def run(self, iterable=None):
        it = iter(iterable)
        first = None
        with open(self.filename, "wb") as csvfile:
            if not self.fieldnames:
                first = it.next()
                self.fieldnames = first.keys()
            writer = DictWriter(csvfile, self.fieldnames, extrasaction="ignore")
            writer.writeheader()
            if first:
                writer.writerow(first)
            for bucket in bucketize(iterable, self.bulk_size):
                writer.writerows(bucket)