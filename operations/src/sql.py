#!/usr/bin/python
# -*- coding: utf-8 -*-

import re

from MySQLdb.cursors import SSDictCursor
from operations.src.base import Operation
from sqlobject.mysql.mysqlconnection import MySQLConnection
from sqlobject.sqlbuilder import Select, Insert
from .utils import bucketize


class Replace(Insert):
    def __sqlrepr__(self, db):
        insert = Insert.__sqlrepr__(self, db)
        return re.sub(r'^INSERT', r'REPLACE', insert)

def fix_params(query):
    return re.sub(r':(\w+)', r'%(\1)s', query)

class SQLOperation(Operation):

    default_host = "mysql://localhost/test"

    @classmethod
    def using(cls, host):
        cls.default_host = host

    def __init__(self, db, **config):
        super(SQLOperation, self).__init__(**config)
        db = db or self.default_host
        self.uri = db.replace("mysql+mysqldb", "mysql")

    def execute(self, cursor, query, params):
        return cursor.execute(fix_params(query), params)

class SQLQuery(SQLOperation):
    def __init__(self, query, db=None, params=None, **config):
        super(SQLQuery, self).__init__(db=db, **config)
        self.query = query
        self.params = params or {}

    def run(self, iterator=None):

        connection = MySQLConnection.connectionFromURI(self.uri)
        cursor = connection.getConnection().cursor(SSDictCursor)

        self.execute(cursor, self.query, self.params)

        for row in cursor:
            yield row

class SQLSelect(SQLOperation):

    def __init__(self, tables, db=None, items="*", **config):
        super(SQLSelect, self).__init__(db=db, **config)
        tables = tables if isinstance(tables, list) else [tables]
        self.select = Select(items, staticTables=tables)
        self.params = {}

    def items(self, items, **params):
        self.select = self.select.newItems(items)
        self.params.update(params)
        return self

    def where(self, clause, **params):
        self.select = self.select.newClause(clause)
        self.params.update(params)
        return self

    def limit(self, limit):
        self.select = self.select.clone(limit=limit)
        return self

    def order_by(self, order):
        self.select = self.select.orderBy(order)
        return self

    def run(self, iterator=None):

        connection = MySQLConnection.connectionFromURI(self.uri)
        cursor = connection.getConnection().cursor(SSDictCursor)

        query = connection.sqlrepr(self.select)
        self.execute(cursor, query, self.params)

        for row in cursor:
            yield row

        connection.close()

class SQLInsert(SQLOperation):

    def __init__(self, table, db=None, bulk_size=100, **config):
        super(SQLInsert, self).__init__(db=db, **config)
        self.table = table
        self.bulk_size = bulk_size
        self.connection = MySQLConnection.connectionFromURI(self.uri)

    def query(self, query):
        self.connection.query(query)

    def run(self, iterable=None):
        for bucket in bucketize(iterable, self.bulk_size):
            keys = list(set(sum([item.keys() for item in bucket], [])))
            values = [{ k:item.get(k) for k in keys } for item in bucket]
            replace = Insert(self.table, valueList=values)
            query = self.connection.sqlrepr(replace)
            self.connection.query(query)
            print "SQLInsert: %s..." % query[:60]

        self.connection.close()

class SQLReplace(SQLOperation):

    def __init__(self, table, db=None, bulk_size=100, **config):
        super(SQLReplace, self).__init__(db=db, **config)
        self.table = table
        self.bulk_size = bulk_size
        self.connection = MySQLConnection.connectionFromURI(self.uri)

    def query(self, query):
        self.connection.query(query)

    def run(self, iterable=None):

        for bucket in bucketize(iterable, self.bulk_size):
            keys = list(set(sum([item.keys() for item in bucket], [])))
            values = [{ ("`%s`" % k):item.get(k) for k in keys } for item in bucket]
            replace = Replace(self.table, valueList=values)
            query = self.connection.sqlrepr(replace)
            self.connection.query(query)
            #print "SQLReplace: %s..." % query[:60]

        self.connection.close()
