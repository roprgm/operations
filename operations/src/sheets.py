
from base import Operation
from utils import bucketize
from collections import OrderedDict

import gspread
from oauth2client.client import SignedJwtAssertionCredentials, AccessTokenCredentials

SHEETS_CLIENT_EMAIL = "skydoms-db@skydoms-992.iam.gserviceaccount.com"
SHEETS_PRIVATE_KEY = "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCBpJINo9Gzhh3O\n0R4TuJvPyymQuDHnPHZ7pszag+HylP9J75V3qObU89GqcJWgk25rNEqh3+GQmUCa\ntFz3mBGa64bVEuEqU/RcQZZEMXZ/qUN5/OWZCzEN5/KPpabqPtRLkjpS730eJpdb\nOBtnBWQa4vAZ86Rk4ScRMFRswDwSFKZVokjmGG/m5tJX519uldXJvdyxlLL8wwvj\nwt7nA0jE9MAznjvjxBoA6uy5E8yuPbeulI1ZVXMVqlb7An/0BsGkBGmJe61g1+3E\n8uNiJqwTLO163V/QTnmYBoD+KHrUlZ3Bfok284nvRUQlMyXmZ/IID3tZ2s4V8dMc\nHzv4RZBXAgMBAAECggEAK0AkGbjVdPcBasYJKrskACbfLxDEVUiunoH/RcdJ0gwz\nPw9W5hzLJbBR+nKk+HU3GTzQyUBPmFYIX5Rcj52EAhMhYGR0ChMIKt8Wqpl11xE5\nGbFtR6mcK+991L6G+Q8M9ElMe+655x0o0Z3eRnhd9TpQr9fAuIcE6Y8eG0nmwID5\ng70nkoYA7Tua3nobGptVAhuo/B1VO3y2dWvdOXv6ckFSnqajPX14NTdFeiW/RpIC\nN4Qbwb5Lz4DEs16yyWC6hrBHhS8R4XfhuHTHa/gYOvpWX60ysLmhEg+gJbXRCek9\npR4Y9TlDORznOlH62JTteL5fX6FbA5yFxn0orhv0AQKBgQC7Had8x2s3XsOkEH49\n2DgKcQ1l45MkVf25eLwdZG2rkBzu/lXOQWBMHL53cci3nRFXoLajx4AJzXN4ej4f\nI3pLVVTOr0KH42uqCrSxM02XoCCHimwJvNn3m5s6dL4uUamXEVf/hiK+rimscjVD\nw8jU2c5Ldj5W2CGlSKpB2cKkwQKBgQCxXn5dZ9x717QuQSuST5H5Flz3yndg1lUV\nIgfBWpLCChXdAse0HpaBtMhjFwP4WoB7S1V9l+XtAgB9yYJEznJgTT+Av4s2WB8j\nCGGazbfHIr5C+gyFB2ou24hhi3/zhzRAOH18cb/tmneqiTNm7eNUGTpBDdkKDsva\nnKArKfqDFwKBgHppxRgZ8qevxHhCHgThvo/+NBCzWKEN7Mb5GMB14lW6G2OzWnls\nRc6lg2nrM7AD/sOALdxp+PbwxRlNh/ShJjHAqpHpELKL8vS0uk+vS5TlukJSKaoZ\nNdQKpfwRQhZVcIiXXSWbIIH0uOjLctNZc7rkn1S5D8GK9kAGkPQxvWhBAoGAKhQ8\n7nQBfvABsyeK6r8t5mZD0BWclMDElqroJBsUziwOW8yZDEZldogpMjj3nCf8a0PI\nGG+9AKmE7FKuMRy5TdeFxUmsAUyOtoT6gvtjKj833DPuXHER4+P5SwsJZAEjiEje\nnHNwETKgSTkblMPDNuR1SZG7eANioY6fAR6IbCkCgYBLyHdwTvD06zYQVpr3CSak\nN9kWXSc2eVpz/1Qg8DyOKZVjGwZUCRYupNFMQTbO+S94IAfWqWOM6QqnjPXtslrG\nb0D9UnLRhl4i/Go2aY52TTc57/aCxt/NnpTkWRpY6s4BETDWDtJLc9SQvTFbOQmf\nNXPG+e0VffwfqibHzbe2iQ==\n-----END PRIVATE KEY-----\n"

def update_docs(wks, docs, primary_key=None, bulk_size=15, upsert=True, n_row=2):

    headers = wks.row_values(1)

    key_index = headers.index(primary_key)

    rows_by_key = OrderedDict()
    for doc in docs:
        rows_by_key[doc[primary_key]] = [doc.get(k) for k in headers]

    keys = wks.col_values(key_index+1)[1:]

    sorted_rows = []
    for key in keys:
        row = rows_by_key.pop(key, [None] * len(headers))
        sorted_rows.append(row)

    if upsert:
        print "Upsert rows: %i" % len(rows_by_key)
        sorted_rows.extend(rows_by_key.values())

    for bucket in bucketize(sorted_rows, bulk_size):

        start_cell = wks.get_addr_int(n_row, 1)
        end_cell = wks.get_addr_int(n_row+len(bucket)-1, len(headers))
        cell_range = "%s:%s" % (start_cell, end_cell)

        cells = wks.range(cell_range)
        cells_to_update = []

        values = []
        for row in bucket:
            values.extend(row)

        for cell, value in zip(cells, values):
            if value and cell.value != value:
                cell.value = value
                cells_to_update.append(cell)

        print "Updating %i/%i cells %s" % (len(cells_to_update), len(cells), cell_range)
        wks.update_cells(cells_to_update)

        n_row += len(bucket)


def export_docs(wks, docs, bulk_size=15, n_row=2):

    headers = wks.row_values(1)

    for bucket in bucketize(docs, bulk_size):

        start_cell = wks.get_addr_int(n_row, 1)
        end_cell = wks.get_addr_int(n_row+len(bucket)-1, len(headers))
        cell_range = "%s:%s" % (start_cell, end_cell)

        cells = wks.range(cell_range)
        cells_to_update = []

        values = []
        for doc in bucket:
            values.extend([doc[k] for k in headers])

        for cell, value in zip(cells, values):
            if value and cell.value != value:
                cell.value = value
                cells_to_update.append(cell)

        print "Updating %i/%i cells %s" % (len(cells_to_update), len(cells), cell_range)
        wks.update_cells(cells_to_update)

        n_row += len(bucket)


def get_worksheet(sheet_name):
    scope = ['https://spreadsheets.google.com/feeds']
    credentials = SignedJwtAssertionCredentials(SHEETS_CLIENT_EMAIL, SHEETS_PRIVATE_KEY.encode(), scope)
    gc = gspread.authorize(credentials)
    return gc.open(sheet_name).sheet1

class SheetUpdate(Operation):
    def __init__(self, sheet_name, primary_key="id", bulk_size=10, upsert=True, **config):
        super(SheetUpdate, self).__init__(**config)
        self.sheet_name = sheet_name
        self.primary_key = primary_key
        self.upsert = upsert
        self.bulk_size = bulk_size

    def run(self, iterable=None):
        wks = get_worksheet(self.sheet_name)
        update_docs(wks, iterable, primary_key=self.primary_key, bulk_size=self.bulk_size, upsert=self.upsert)

class SheetExport(Operation):
    def __init__(self, sheet_name, bulk_size=10, **config):
        super(SheetExport, self).__init__(**config)
        self.sheet_name = sheet_name
        self.bulk_size = bulk_size

    def run(self, iterable=None):
        wks = get_worksheet(self.sheet_name)
        export_docs(wks, iterable, bulk_size=self.bulk_size)