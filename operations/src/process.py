import multiprocessing as mp
from itertools import izip, cycle

from base import Operation
from utils import bucketize

class Process(Operation):
    def __init__(self, func,  **config):
        super(Process, self).__init__(**config)
        self.func = func

    def process(self, doc):
        return self.func(doc)

    def run(self, iterable=None):
        for x in iter(iterable):
            result = self.process(x)
            if not result is None:
                yield result

class ProcessingWorker(object):
    def __init__(self, operation):
        self.operation = operation
        self.in_queue = mp.Queue(1)
        self.out_queue = mp.Queue(1)

        def run(op, in_queue, out_queue):
            for result in op.run(iter(in_queue.get, StopIteration)):
                out_queue.put(result)

        self.process = mp.Process(target=run, args=(self.operation, self.in_queue, self.out_queue))

    def start(self):
        self.process.daemon = True
        self.process.start()
        print "Started", self.process, self.process.pid

    def stop(self):
        print "Stopping", self.process, self.process.pid
        self.in_queue.put(StopIteration)

    def put(self, item):
        self.in_queue.put(item)

    def get(self):
        return self.out_queue.get()

class MultiProcessSplit(Operation):

    def __init__(self, operations, **config):
        super(MultiProcessSplit, self).__init__(**config)
        self.operations = operations
        assert isinstance(operations, list)

    def run(self, iterable=None):

        workers = [ProcessingWorker(op) for op in self.operations]

        map(ProcessingWorker.start, workers)

        for item in iterable:
            for worker in workers:
                worker.put(item)
            for worker in workers:
                result = worker.get()
                yield result

        map(ProcessingWorker.stop, workers)

class MultiProcessHub(Operation):

    def __init__(self, operations, workers=None, **config):
        super(MultiProcessHub, self).__init__(**config)

        if isinstance(operations, Operation):
            operations = [operations]

        self.operations = list(operations)

        if workers:
            it = cycle(operations)
            while len(self.operations) < workers:
                self.operations.append(it.next())

        print self.operations

    def run(self, iterable=None):

        workers = [ProcessingWorker(op) for op in self.operations]

        map(ProcessingWorker.start, workers)

        for bucket in bucketize(iterable, len(workers)):
            for item, worker in izip(bucket, workers):
                worker.put(item)
            for item, worker in izip(bucket, workers):
                result = worker.get()
                yield result

        map(ProcessingWorker.stop, workers)

from base import Map

class MultiProcessMap(MultiProcessHub):
    def __init__(self, func, workers=2, **config):
        super(MultiProcessMap, self).__init__([Map(func)], workers=workers, **config)