#!/usr/bin/python
# -*- coding: utf-8 -*-

from base import Operation

class WikiaReplace(Operation):

    def __init__(self, workers=1):
        super(WikiaReplace, self).__init__()
        self.workers = workers

    def run(self, iterable=None):

        print "run"

        import sys
        reload(sys)
        sys.setdefaultencoding("utf-8")

        from pywikibot import Site, Page
        from pywikibot.throttle import Throttle
        from pywikibot.families.wikia_family import Family

        family = Family()
        family.name = "skydoms"
        family.langs = { "en": "skydoms.wikia.com" }
        family.hostname = lambda code: "skydoms.wikia.com"

        site = Site(fam=family)
        site._throttle = Throttle(site, writedelay=1)

        for item in iterable:
            title = item["title"]
            content = item["content"]
            page = Page(site, title)
            site._throttle.writedelay = 1
            if not page.text.strip() == content.strip():
                page.put(content, force=True)
            yield item
