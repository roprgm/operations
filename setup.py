#!/usr/bin/env python

from setuptools import setup

setup(name='Operations',
    version='1.0',
    description='Python Operations',
    author='Rodrigo Belfiore',
    author_email='rodrigo.b@metrogames.com',
    packages=['operations'],
    install_requires=['pymongo', 'elasticsearch', 'requests', 'sqlobject', 'MySQL-python',
              'smart_open', 'sqlobject', 'pywikibot', 'gspread', 'oauth2client']
)