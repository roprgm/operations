#!/usr/bin/python
# -*- coding: utf-8 -*-

from operations import *
from json import loads

a = ListInput(xrange(5))

def f(n):
    print n
    return n*n

b = Map(f)

r2 = a | Split([Map(lambda x:x),b,Map(lambda x:x**3)])

GetIP = IterInput(["http://httpbin.org/ip"]) | HTTPGet() | Map(loads) | ItemGetter("origin")

Pipeline([
    Split([
        GetIP,
        list(GetIP) * 2,
    ]),
    PrintOutput()
])()